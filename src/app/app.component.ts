import { Component, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './app.module';
import { getHttpStatusError } from './core/store/http-status/http-status.selectors';

@Component({
  selector: 'app-root',
  template: `
    
    <div 
      style="background: red" 
      *ngIf="httpStatusError$ | async"
    >{{httpStatusError$ | async}}</div>
    
    <button routerLink="login">login</button>
    <button routerLink="catalog">catalog</button>
    <button routerLink="home">home</button>
    <button routerLink="pexels-video">pexels</button>
    <button routerLink="items">items</button>
    <hr>
    <router-outlet></router-outlet>
    
  `,
})
export class AppComponent {
  httpStatusError$ = this.store.select(getHttpStatusError)

    constructor(private store: Store<AppState>) {}
}
