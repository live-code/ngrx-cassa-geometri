import { AppState } from '../../../app.module';

export const getHttpStatusError = (state: AppState) => state.httpStatus.error
export const getHttpStatusSuccess = (state: AppState) => state.httpStatus.success
