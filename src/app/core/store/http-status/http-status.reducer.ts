import { createReducer, on } from '@ngrx/store';
import { setError, setSuccess } from './http-status.actions';

export interface HttpStatusState {
  error: string | null;
  success: boolean;
}

const initialState: HttpStatusState = {
  error: null,
  success: false,
}

export const httpStatusReducer = createReducer(
  initialState,
  on(setError, (state, action) => ({ error: action.msg, success: false })),
  on(setSuccess, (state) => ({ error: null, success: true })),
)
