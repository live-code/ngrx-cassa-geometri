import { createAction, props } from '@ngrx/store';

export const setSuccess = createAction('[httpStatus] setSuccess')
export const setError = createAction('[httpStatus] setError', props<{msg: string}>())
