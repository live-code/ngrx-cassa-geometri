import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Store } from '@ngrx/store';
import { first, iif, mergeMap, Observable, take, withLatestFrom } from 'rxjs';
import { AppState } from '../../../app.module';
import { getAuthToken, getIsLogged } from './auth.selectors';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private store: Store<AppState>) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.store.select(getIsLogged)
      .pipe(
        // first(),
        take(1),
        withLatestFrom(this.store.select(getAuthToken)),
        mergeMap(([isLogged, tk]) => iif(
          () => isLogged,
          next.handle( request.clone({ setHeaders: { Authorization: 'Bearer ' + tk}}) ),
          next.handle( request )
        ))
      )
  }


 /* intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.store.select(getAuthToken)
      .pipe(
        // first(),
        take(1),
        mergeMap(tk => iif(
          () => !!tk,
          next.handle( request.clone({ setHeaders: { Authorization: 'Bearer ' + tk}}) ),
          next.handle( request )
        ))
      )
  }*/

/*
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.store.select(getAuthToken)
      .pipe(
        // first(),
        take(1),
        mergeMap(tk => {
          const cloneReq = tk ? request.clone({ setHeaders: { Authorization: 'Bearer ' + tk}}) : request
          return next.handle(cloneReq)
        })
      )
  }*/
}
