import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, of, tap } from 'rxjs';
import { AppState } from '../../../app.module';
import { go } from '../router/router.actions';
import { getIsLogged } from './auth.selectors';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store<AppState>) {}

  canActivate(): Observable<boolean> {

    return this.store.pipe(select(getIsLogged))
      .pipe(
        tap(isLogged => {
          if (!isLogged) {
            this.store.dispatch(go({ path: 'login' }))
          }
        })
      )
  }
  
}
