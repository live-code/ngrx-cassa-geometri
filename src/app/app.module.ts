import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { routerReducer, RouterReducerState, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthEffects } from './core/store/auth/auth.effects';
import { AuthInterceptor } from './core/store/auth/auth.interceptor';
import { authReducer, AuthState } from './core/store/auth/auth.reducer';
import { httpStatusReducer, HttpStatusState } from './core/store/http-status/http-status.reducer';
import { RouterEffects } from './core/store/router/router.effects';

export interface AppState {
  authentication: AuthState,
  httpStatus: HttpStatusState
  router: RouterReducerState
}

export const rootReducer: ActionReducerMap<AppState> = {
  authentication: authReducer,
  httpStatus: httpStatusReducer,
  router: routerReducer
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(rootReducer),
    EffectsModule.forRoot([RouterEffects, AuthEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 15
    }),
    FormsModule,
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal
    })
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
