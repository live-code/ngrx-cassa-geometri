import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { getHttpStatusError } from '../../core/store/http-status/http-status.selectors';
import { UsersFeatureState } from './items.module';
import { addItem, clear, deleteItem, loadItems } from './store/items.actions';
import { ItemsEffects } from './store/items.effects';
import { getItems, getItemsError, getItemsPending } from './store/items.selectors';

@Component({
  selector: 'app-items',
  template: `
    <div *ngIf="pending$ | async">loader....</div>

    <button (click)="loadItemsHandler()">load</button>

    <form #f="ngForm" (submit)="addItemHandler(f.value)">
      <input type="text" ngModel name="name">
    </form>

    <li *ngFor="let item of items$ | async">
      {{item.name}}
      <button (click)="deleteHandler(item.id)">Delete</button>
    </li>


  `,
})
export class ItemsComponent {
  @ViewChild('f') form!: NgForm
  items$ = this.store.select(getItems);
  pending$ = this.store.select(getItemsPending);
  error$ = this.store.select(getItemsError);

  constructor(
    private store: Store<any>,
    private actions: Actions,
    private effects: ItemsEffects
  ) {


    this.effects.addItem$.subscribe(() => this.form.reset())
    /*
    this.actions
      .pipe(
        ofType(addItemSuccess)
      )
      .subscribe(() => {
        this.form.reset();
      })*/
  }

  loadItemsHandler() {
    this.store.dispatch(loadItems())
  }

  addItemHandler(item: any) {
    this.store.dispatch(addItem({ item }))
  }

  deleteHandler(id: number) {
    this.store.dispatch(deleteItem({ id }))
  }

  ngOnDestroy() {
    this.store.dispatch(clear())
  }
}
