import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducerMap, StoreModule } from '@ngrx/store';

import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { ItemsEffects } from './store/items.effects';
import { itemsReducer, UsersState } from './store/items.reducer';

export interface UsersFeatureState {
  items: UsersState,
  clients: any[]
}

const reducers: ActionReducerMap<UsersFeatureState> = {
  items: itemsReducer,
  clients: () => []
}

@NgModule({
  declarations: [
    ItemsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    StoreModule.forFeature('users2', reducers),
    EffectsModule.forFeature([ItemsEffects]),
    ItemsRoutingModule
  ]
})
export class ItemsModule { }
