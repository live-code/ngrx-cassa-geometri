import { createAction, props } from '@ngrx/store';
import { Item } from '../model/item';

export const loadItems = createAction('[items] load');
export const loadItemsSuccess = createAction('[items] load success', props<{ items: Item[]}>());
export const loadItemsFailed = createAction('[items] load failed');

export const addItem = createAction('[item] add', props<{ item: Item }>())
export const addItemSuccess = createAction('[item] add success', props<{ item: Item }>())
export const addItemFailed = createAction('[item] add failed')

export const deleteItem = createAction('[Item] Delete', props<{ id: number }>());
export const deleteItemSuccess = createAction('[Item] Delete Success', props<{ id: number }>());
export const deleteItemFail = createAction('[Item] Delete Item Fail');

export const clear = createAction('[items] clear')

