import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, delay, filter, map, mergeMap, of, tap } from 'rxjs';
import { setError, setSuccess } from '../../../core/store/http-status/http-status.actions';
import { Item } from '../model/item';
import {
  addItem, addItemFailed, addItemSuccess,
  deleteItem,
  deleteItemFail,
  deleteItemSuccess,
  loadItems,
  loadItemsFailed,
  loadItemsSuccess
} from './items.actions';

@Injectable()
export class ItemsEffects {


  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    mergeMap(
      () => this.http.get<Item[]>('https://jsonplaceholder.typicode.com/users')
        .pipe(
          delay(2000),
          mergeMap(items => [loadItemsSuccess({ items }), setSuccess()]),
          //catchError(e => of(loadItemsFailed()))
          catchError(e => of(setError({ msg: ' error caricamento users'})))
        )
    ),
  ))

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    mergeMap(
      ({ id }) => this.http.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
        .pipe(
          map(() => deleteItemSuccess({ id })),
          catchError(() => of(deleteItemFail()))
        )
    )
  ))

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    mergeMap(
      ({ item }) => this.http.post<Item>(`https://jsonplaceholder.typicode.com/users/`, item)
        .pipe(
          mergeMap((item) => [addItemSuccess({ item }), /*clearItems()*/]),
          catchError(() => of(addItemFailed()))
        )
    )
  ))

  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {
    this.actions$.subscribe(console.log)
  }
}
