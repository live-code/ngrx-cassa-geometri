import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../../../app.module';
import { UsersFeatureState } from '../items.module';

export const getUsersFeatureSelector = createFeatureSelector<UsersFeatureState>('users2');

export const getItems = createSelector(
  getUsersFeatureSelector,
  (state: UsersFeatureState) => state.items.list
)

export const getItemsError = createSelector(
  getUsersFeatureSelector,
  (state) => state.items.error
);

export const getItemsPending = createSelector(
  getUsersFeatureSelector,
  (state) => state.items.pending
);
