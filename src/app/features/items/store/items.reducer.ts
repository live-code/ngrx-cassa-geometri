import { state } from '@angular/animations';
import { createReducer, on } from '@ngrx/store';
import { Item } from '../model/item';
import {
  addItem, addItemFailed,
  addItemSuccess, clear,
  deleteItem, deleteItemFail,
  deleteItemSuccess, loadItems,
  loadItemsFailed,
  loadItemsSuccess
} from './items.actions';


export interface UsersState {
  list: Item[];
  pending: boolean;
  error: boolean;
}

const initialState: UsersState = {
  list: [],
  pending: false,
  error: false,
}

export const itemsReducer = createReducer(
  initialState,
  on(loadItems, (state) => ({...state, pending: true, error: false})),
  on(loadItemsSuccess, (state, action) => ({
    ...state,
    pending: false,
    list: [...action.items] })
  ),
  on(loadItemsFailed, (state) => ({...state, list: [], error: true, pending: false })),

  on(addItemSuccess, (state, action) => ({
    ...state,
    list: [...state.list, action.item]})
  ),
  on(addItemFailed, (state) => ({...state, error: true })),

  on(deleteItemSuccess, (state, action) => ({
    ...state,
    list: state.list.filter(item => item.id !== action.id)})
  ),
  on(deleteItemFail, (state) => ({...state, error: true })),
  on(clear, () => ({...initialState}))
)



/*

const initialState: Item[] = [];

export const itemsReducer = createReducer(
  initialState,
  on(loadItemsSuccess, (state, action) => [...action.items]),
  on(addItemSuccess, (state, action) => [...state, action.item]),
  on(deleteItemSuccess, (state, action) => state.filter(item => item.id !== action.id)),
  on(clearItems, () => [])
)
*/
