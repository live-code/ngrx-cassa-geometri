import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LetModule } from '@ngrx/component';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { PexelsVideoRoutingModule } from './pexels-video-routing.module';
import { PexelsVideoComponent } from './pexels-video.component';
import { reducers } from './store';
import { PexelsVideoSearchEffects } from './store/search/pexels-video-search.effects';

// BARREL FILES

@NgModule({
  declarations: [
    PexelsVideoComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LetModule,
    PexelsVideoRoutingModule,
    EffectsModule.forFeature([ PexelsVideoSearchEffects ]),
    StoreModule.forFeature('pexels-video', reducers)
  ]
})
export class PexelsVideoModule { }
