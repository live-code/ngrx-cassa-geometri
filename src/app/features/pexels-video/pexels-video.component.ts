import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { debounceTime, distinctUntilChanged, first, Observable } from 'rxjs';
import { go } from '../../core/store/router/router.actions';
import { Video } from '../../model/pexels-video-response';
import { filterByMinResolution } from './store/filters/pexels-video-filters.actions';
import { showVideo } from './store/player/player.actions';
import { searchVideos } from './store/search/pexels-video-search.actions';
import { PexelsVideoSearchState } from './store/search/pexels-video-search.reducer';
import {
  getPexelsVideos,
  getPexelsVideoLink,
  getPexelsVideoPictures, getPexelsVideoSearchText
} from './store/search/pexels-video-search.selectors';

@Component({
  selector: 'app-pexels-video',
  template: `
    <input type="text" [formControl]="searchInput"> <br>
    
    <ng-container *ngrxLet="videoLink$ as v; suspenseTpl: msg">
      <div
        *ngIf="v"
        style="padding: 10px; border: 2px solid black"
      >
        <video [src]="v" width="200" controls></video>
      </div>
    </ng-container>
    
    <ng-template #msg>
      loading
    </ng-template>
    
    
    
    <div style="display: flex" *ngIf="videoPictures$ | async as pics">
      <br>
      <img *ngFor="let v of pics" [src]="v" alt="" width="50">
    </div>

    <hr>

    <!--NEW-->
    <select (change)="filterByResolution($event)">
      <option>0</option>
      <option>2000</option>
      <option>3000</option>
      <option>4000</option>
    </select>
    
    <div *ngFor="let video of videos$ | async" (click)="showPreview(video)">
      <img [src]="video.image" alt="" width="100">
      {{video.width}}x{{video.height}}
    </div>
    
  `,
})
export class PexelsVideoComponent {
  searchInput = new FormControl('', { nonNullable: true });
  videos$ = this.store.select(getPexelsVideos);
  text$ = this.store.select(getPexelsVideoSearchText);
  videoPictures$ = this.store.select(getPexelsVideoPictures)
  videoLink$ = this.store.select(getPexelsVideoLink)

  constructor(private store: Store<PexelsVideoSearchState>) {
    this.searchInput.valueChanges
      .pipe(
        debounceTime(700),
        distinctUntilChanged()
      )
      .subscribe(text => {
        store.dispatch(searchVideos({ text }))
      })

    this.text$
      .pipe(first())
      .subscribe(text => {
        this.searchInput.setValue(text, { emitEvent: false})
      })


  }

  showPreview(video: Video) {
    this.store.dispatch(showVideo({ video }))
  }
  filterByResolution(event: Event): void {
    const minResolution = +(event.target as HTMLSelectElement).value;
    this.store.dispatch(filterByMinResolution({ minResolution }));
  }

}
