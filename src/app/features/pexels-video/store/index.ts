import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import { filtersReducer, PexelsFilterState } from './filters/pexels-video-filters.reducer';
import { PexelsPlayerState, playerReducer } from './player/player.reducer';
import { PexelsVideoSearchState, searchReducer } from './search/pexels-video-search.reducer';


export interface PexelsVideoState {
  search: PexelsVideoSearchState;
  player: PexelsPlayerState,
  filters: PexelsFilterState;
}

export const reducers: ActionReducerMap<PexelsVideoState> = {
  search: searchReducer,
  player: playerReducer,
  filters: filtersReducer
}

