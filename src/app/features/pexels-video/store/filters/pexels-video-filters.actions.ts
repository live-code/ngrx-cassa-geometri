import { createAction, props } from '@ngrx/store';

export const filterByMinResolution = createAction(
  '[pexels-filters] by min resolution',
  props<{ minResolution: number}>()
);
