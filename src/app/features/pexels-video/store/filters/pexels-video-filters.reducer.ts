import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { PexelsVideoState } from '../index';
import { filterByMinResolution } from './pexels-video-filters.actions';

export interface PexelsFilterState {
  minResolution: number;
  /*text: number;
  colors: number;*/
}

export const initialState: PexelsFilterState = {
  minResolution: 0,
};

export const filtersReducer = createReducer(
  initialState,
  on(filterByMinResolution, (state, action) => ({ minResolution: action.minResolution})),
);


// selectors
export const getItemFeature = createFeatureSelector<PexelsVideoState>('pexels-video');


export const getFilters = createSelector(
  getItemFeature,
  state => state.filters
);

