import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../../../../app.module';
import { getHttpStatusSuccess } from '../../../../core/store/http-status/http-status.selectors';
import { getFilters } from '../filters/pexels-video-filters.reducer';
import { PexelsVideoState } from '../index';
import { getCurrentVideo } from '../player/player.selectors';

export const getVideoPexelsFeature = createFeatureSelector<PexelsVideoState>('pexels-video');

export const getPexelsSearch = createSelector(
  getVideoPexelsFeature,
  (state) => state.search
);

export const getPexelsVideos = createSelector(
  getPexelsSearch,
  getFilters,
  (search, filters) => search.list.filter(v => v.width > filters.minResolution)
);


export const getPexelsVideoSearchPending = createSelector(
  getVideoPexelsFeature,
    state => state.search.pending
);

export const getPexelsVideoSearchText = createSelector(
  getVideoPexelsFeature,
    state => state.search.text
);
export const getPexelsVideoSearchError = createSelector(
  getVideoPexelsFeature,
    state => state.search.hasError
);

export const getPexelsVideoPictures = createSelector(
  getPexelsVideos,
  getCurrentVideo,
  (videos, currentVideo) => videos.find(v => v.id === currentVideo?.id)?.video_pictures
    .map(image => image.picture )

);


export const getPexelsVideoLink = createSelector(
  getPexelsVideos,
  getCurrentVideo,
  (videos, active) => videos.find(v => v.id === active?.id)?.video_files[0].link
);
