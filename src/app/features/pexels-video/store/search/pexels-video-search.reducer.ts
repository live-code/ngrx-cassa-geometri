import { createReducer, on } from '@ngrx/store';
import { Video } from '../../../../model/pexels-video-response';
import { showVideo } from '../player/player.actions';
import { searchVideos, searchVideosFails, searchVideosSuccess } from './pexels-video-search.actions';

export interface PexelsVideoSearchState {
  list: Video[];
  hasError: boolean;
  pending: boolean;
  text: string;
}

export const initialState: PexelsVideoSearchState  = {
  list: [],
  hasError: false,
  pending: false,
  text: ''
}

export const searchReducer = createReducer(
  initialState,
  on(searchVideos, (state, { text }) => ({...state,  pending: true, text })),
  on(searchVideosSuccess, (state, action) => ({...state, list: action.items, pending: false, hasError: false })),
  on(searchVideosFails, (state) => ({...state, pending: false, hasError: true })),
  on(showVideo, (state, action) => ({...state, selectedVideo: action.video}))
)
