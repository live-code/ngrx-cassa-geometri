import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { PexelsVideoResponse } from '../../../../model/pexels-video-response';
import { searchVideos, searchVideosFails, searchVideosSuccess } from './pexels-video-search.actions';

@Injectable()
export class PexelsVideoSearchEffects {

  searchVideo$ = createEffect(() => this.actions$.pipe(
    ofType(searchVideos),
    switchMap(
      action => {
        return this.http.get<PexelsVideoResponse>(
          'https://api.pexels.com/videos/search?per_page=10&query=' + action.text,
          { headers: { Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be' } }
        ).pipe(
          map(response => searchVideosSuccess({ items: response.videos })),
          catchError(() => of(searchVideosFails()))
        );
      }
    )
  ))

  constructor(private actions$: Actions, private http: HttpClient) {
  }
}
