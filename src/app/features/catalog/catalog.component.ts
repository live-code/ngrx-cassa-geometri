import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadItems } from '../items/store/items.actions';
import { ProductsActions } from './store/products.actions';
import { getProductList } from './store/products.feature';

@Component({
  selector: 'app-catalog',
  template: `
    <p>
      catalog works!
      
    </p>
    <button (click)="loadItemsHandler()">load</button>

    <pre>{{(products$ | async)?.length}}</pre>
  `,
  styles: [
  ]
})
export class CatalogComponent {
  products$ = this.store.select(getProductList)

  constructor(private store: Store<any>) {

  }


  loadItemsHandler() {
    this.store.dispatch(ProductsActions.load())
  }
}
