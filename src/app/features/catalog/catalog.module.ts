import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducerMap, StoreModule } from '@ngrx/store';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { ProductsEffects } from './store/products.effects';
import { productReducer, productsFeature, ProductsState } from './store/products.feature';

export interface CatalogState {
  products: ProductsState,
  filters: any;
  active: any;
}

export const reducers: ActionReducerMap<CatalogState> = {
  products: productReducer,
  filters: () => ({}),
  active: () => 123
}


@NgModule({
  declarations: [
    CatalogComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    EffectsModule.forFeature([ ProductsEffects]),
    StoreModule.forFeature('catalog', reducers)
  ]
})
export class CatalogModule { }
