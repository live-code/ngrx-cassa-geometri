import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Item } from '../../items/model/item';

export const ProductsActions = createActionGroup({
  source: 'Products',
  events: {
    'Load': emptyProps(),
    'Load Success': props<{ items: Item[] }>(),
    'Load Fail': emptyProps(),

    'add Item': props<{ item: Item }>(),
    'add Item Success':  props<{ item: Item }>(),
    'add Item Fail': emptyProps(),

    'Delete Item': props<{ id: number }>(),
    'Delete Item Success': props<{ id: number }>(),
    'Delete Item Fail': emptyProps(),
  }
});

