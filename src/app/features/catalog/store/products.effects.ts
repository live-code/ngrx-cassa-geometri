// catalog/store/products.effect.ts
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { mergeMap, switchMap, catchError, map, exhaustMap, concatMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { delay, of } from 'rxjs';
import { Item } from '../../items/model/item';

import { ProductsActions } from './products.actions';


@Injectable()
export class ProductsEffects {

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(ProductsActions.load),
    switchMap(() =>
      this.http.get<Item[]>('https://jsonplaceholder.typicode.com/users')
        .pipe(
          delay(1000),
          map(data => ProductsActions.loadSuccess({ items: data})),
          catchError(() => of(ProductsActions.loadFail())
          )
        )
    )
  ));

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(ProductsActions.deleteItem),
    exhaustMap((action) =>
      this.http.delete(`https://jsonplaceholder.typicode.com/users/${action.id}`)
        .pipe(
          map(() => ProductsActions.deleteItemSuccess({ id: action.id})),
          catchError(() => of(ProductsActions.deleteItemFail())
          )
        )
    )
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(ProductsActions.addItem),
    concatMap((action) =>
      this.http.post<Item>(`https://jsonplaceholder.typicode.com/users/`, action.item)
        .pipe(
          map((item) => ProductsActions.addItemSuccess({ item })),
          catchError(() => of(ProductsActions.addItemFail())
          )
        )
    )
  ));

  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {}
}
