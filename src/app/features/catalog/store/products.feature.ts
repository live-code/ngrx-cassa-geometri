import { createFeature, createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { getHttpStatusError } from '../../../core/store/http-status/http-status.selectors';
import { Item } from '../../items/model/item';
import { CatalogState } from '../catalog.module';
import { ProductsActions } from './products.actions';


// UPDATE
export interface ProductsState {
  hasError: boolean;
  list: Item[];
}

// UPDATE
export const initialState: ProductsState = {
  hasError: false,
  list: [],
};

export const productsFeature = createFeature({
  name: 'products',
  reducer: createReducer(
    initialState,
    on(ProductsActions.load, (state) => ({ ...state, hasError: false})),
    on(ProductsActions.loadSuccess, (state, action) => ({list: [...action.items], hasError: false})),
    on(ProductsActions.loadFail, (state) => ({ ...state, hasError: true})),

    on(ProductsActions.deleteItem, (state) => ({ ...state, hasError: false})),
    on(ProductsActions.deleteItemSuccess, (state, action) => ({ list: state.list.filter(item => item.id !== action.id), hasError: false})),
    on(ProductsActions.deleteItemFail, (state) => ({ ...state, hasError: true})),

    on(ProductsActions.addItem, (state) => ({ ...state, hasError: false})),
    on(ProductsActions.addItemSuccess, (state, action) => ({ list: [...state.list, action.item], hasError: false})),
    on(ProductsActions.addItemFail, (state) => ({ ...state, hasError: true})),
  ),
})

export const {
  name,
  reducer: productReducer,
  selectList,
  selectHasError
} = productsFeature;



// ==================
export const getCatalogFeatureSelector = createFeatureSelector<CatalogState>('catalog');

export const getProductList = createSelector(
  getCatalogFeatureSelector,
  state => state.products.list
)

export const getSOmeting = createSelector(
  getCatalogFeatureSelector,
  (catalog ) => catalog.products.list
)
